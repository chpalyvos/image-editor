#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    central_widget(new QWidget),
    layout(new QGridLayout),
    welcome_button(new QPushButton("Click here to start")),
    welcome_label(new QLabel),
    gif(new QMovie("/home/christos/workspace/image-editor/ImageEditor/data/welcome.gif"))
{
    ui->setupUi(this);
    set_gif_to_label();
    set_ui();
    connect(welcome_button, SIGNAL(clicked()), this, SLOT(openImgWindow()));
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::openImgWindow(){
    this->close();
    ImgWindow img_win;
    img_win.setModal(true);
    img_win.setWindowTitle("Image Editor");
    img_win.exec();
}

void MainWindow::set_gif_to_label(){
    if (! gif->isValid()){
        welcome_label->setText("W-E-L-C-O-M-E");
        return;
    }
    welcome_label->setMovie(gif);
    welcome_label->setScaledContents(true);
    gif->start();
}

void MainWindow::set_ui(){
    // add widgets to layout
    layout->addWidget(welcome_label,0,0);
    layout->addWidget(welcome_button,1,0);
    central_widget->setLayout(layout);
    setCentralWidget(central_widget);
}
