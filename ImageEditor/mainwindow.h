#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QString>
#include <QPushButton>
#include <QMovie>
#include <QGridLayout>
#include <QLabel>
#include <imgwindow.h>


namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
private slots:
    void openImgWindow();
private:
    Ui::MainWindow *ui;
    QWidget *central_widget;
    QGridLayout *layout;
    QPushButton *welcome_button;
    QLabel *welcome_label;
    QMovie *gif;
    void set_ui();
    void set_gif_to_label();
};

#endif // MAINWINDOW_H
