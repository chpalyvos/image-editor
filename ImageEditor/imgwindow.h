#ifndef IMGWINDOW_H
#define IMGWINDOW_H

#include <QDialog>
#include <QFileDialog>
#include <QDebug>
#include <QImageReader>
#include <QPixmap>
#include <QLabel>
#include <QImage>
#include <QGridLayout>
#include <QVBoxLayout>
#include <QPushButton>
#include <QSlider>
#include "boost/lexical_cast.hpp"
#include "opencv2/core/core.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include <stack>

namespace Ui {
class ImgWindow;
}


class ImgWindow : public QDialog
{
    Q_OBJECT
private slots:
    void img_load();
    void img_to_gray();
    void img_save();
    void set_img_brightness();
    void set_previous_image();
public:
    explicit ImgWindow(QWidget *parent = 0);
    ~ImgWindow();
    static int img_index;
    static QImage cvImg_to_QImg (cv::Mat const& cv_img);
    static cv::Mat QImg_to_cvImg(QImage const& qimg);
private:
    Ui::ImgWindow *ui;
    QWidget *central_widget;
    QGridLayout *layout;
    QVBoxLayout *utilities_layout;
    QLabel *img_label;
    QPushButton *img_load_button;
    QPushButton *img_to_gray_button;
    QPushButton *img_save_button;
    QSlider *brightness_slider;
    QPushButton *undo_button;
    QImage img;
    QImage img_bk;
    std::stack<QImage> image_stack;
    QFileDialog *fl_dialog;
    QString fn;
    int img_brightness;
    void set_ui();
    void set_image_from_dialog();
    void set_slider();
    void set_image();
    void set_image(bool);
};

#endif // IMGWINDOW_H
