#include "imgwindow.h"
#include "ui_imgwindow.h"

int ImgWindow::img_index = 0;


ImgWindow::ImgWindow(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ImgWindow),
    central_widget(new QWidget),
    layout(new QGridLayout),
    utilities_layout(new QVBoxLayout),
    img_label(new QLabel),
    img_load_button(new QPushButton("Load Image")),
    img_to_gray_button(new QPushButton("To Gray Scale")),
    img_save_button(new QPushButton("Save Image")),
    brightness_slider(new QSlider(Qt::Horizontal)),
    undo_button(new QPushButton("Undo")),
    fl_dialog(new QFileDialog(this)),
    fn(""),
    img_brightness(0)
{
    ui->setupUi(this);
    set_ui();
    set_slider();
    connect(img_load_button ,SIGNAL(clicked(bool)), this, SLOT(img_load()));
    connect(img_to_gray_button, SIGNAL(clicked(bool)), this, SLOT(img_to_gray()));
    connect(img_save_button, SIGNAL(clicked(bool)), this, SLOT(img_save()));
    connect(brightness_slider, SIGNAL(sliderReleased()), this, SLOT(set_img_brightness()));
    connect(undo_button, SIGNAL(clicked(bool)), this, SLOT(set_previous_image()));
}

ImgWindow::~ImgWindow()
{
    delete ui;
}

void ImgWindow::img_load(){
    fl_dialog->setFileMode(QFileDialog::ExistingFile);
    fl_dialog->setNameFilter(tr("Images (*.png *.xpm *.jpg)"));
    fl_dialog->setViewMode(QFileDialog::Detail);
    QStringList filename;
    int count = 0;
    bool done = false;
    do{
        if (fl_dialog->exec() == QFileDialog::Accepted){
            filename = fl_dialog->selectedFiles();
        }
        if (!filename.isEmpty()){
            fn = filename[0];
        }
        if (++count == 1) done = true;

    }while(!done);
    set_image_from_dialog();
}

void ImgWindow::set_image_from_dialog(){
    img.load(fn);
    qInfo() << fn ;
    if (img.isNull()){
        img_label->setText("File Not Found");
        return;
    }
    set_image();
}

void ImgWindow::img_to_gray(){
    int W = img.width();
    int H = img.height();
    int D = img.depth()/8;
    int i, j;
    for (i = 0; i < H ;i++){
        uchar* line = img.scanLine(i);
        for (j=0; j < W; j++){
            QRgb* rgbPixel = reinterpret_cast<QRgb*> (line + j*D);
            int gray = qGray(*rgbPixel);
            *rgbPixel = QColor(gray, gray, gray).rgba();
            img.setPixel(j,i,*rgbPixel);
        }
    }
    set_image();
}

void ImgWindow::set_ui(){
    // add grid layout items
    layout->addWidget(img_label, 0, 0);
    layout->addLayout(utilities_layout,0,1);
    // add vertical layout items
    utilities_layout->addWidget(img_load_button);
    utilities_layout->addWidget(img_to_gray_button);
    utilities_layout->addWidget(img_save_button);
    utilities_layout->addWidget(brightness_slider);
    utilities_layout->addWidget(undo_button);
    setLayout(layout);
}

void ImgWindow::img_save(){
    std::string save_path = "/home/christos/workspace/image-editor/ImageEditor/output/image-editor-";
    std::string save_name =
            save_path
            +
            boost::lexical_cast<std::string>(++img_index)
            +
            ".png";

    QString save_fname = QString::fromStdString(save_name);
    bool saved = img.save(save_fname, "png", 0);
    if (! saved){
        qInfo() << "Error while saving" ;
    }
}

QImage ImgWindow::cvImg_to_QImg(const cv::Mat &mat){
    // 8-bits unsigned, NO. OF CHANNELS = 1
        if(mat.type() == CV_8UC1)
        {
            QImage image(mat.cols, mat.rows, QImage::Format_Indexed8);
            // Set the color table (used to translate colour indexes to qRgb values)
            image.setColorCount(256);
            for(int i = 0; i < 256; i++)
            {
                image.setColor(i, qRgb(i, i, i));
            }
            // Copy input Mat
            uchar *pSrc = mat.data;
            for(int row = 0; row < mat.rows; row ++)
            {
                uchar *pDest = image.scanLine(row);
                memcpy(pDest, pSrc, mat.cols);
                pSrc += mat.step;
            }
            return image;
        }
        // 8-bits unsigned, NO. OF CHANNELS = 3
        else if(mat.type() == CV_8UC3)
        {
            // Copy input Mat
            const uchar *pSrc = (const uchar*)mat.data;
            // Create QImage with same dimensions as input Mat
            QImage image(pSrc, mat.cols, mat.rows, mat.step, QImage::Format_RGB888);
            return image.rgbSwapped();
        }
        else if(mat.type() == CV_8UC4)
        {
            qDebug() << "CV_8UC4";
            // Copy input Mat
            const uchar *pSrc = (const uchar*)mat.data;
            // Create QImage with same dimensions as input Mat
            QImage image(pSrc, mat.cols, mat.rows, mat.step, QImage::Format_ARGB32);
            return image.copy();
        }
        else
        {
            qDebug() << "ERROR: Mat could not be converted to QImage.";
            return QImage();
        }
}

cv::Mat ImgWindow::QImg_to_cvImg(const QImage &qimg){
    cv::Mat mat;
    //qDebug() << qimg.format();
    switch(qimg.format())
    {
    case QImage::Format_ARGB32:
    case QImage::Format_RGB32:
    case QImage::Format_ARGB32_Premultiplied:
        mat = cv::Mat(qimg.height(), qimg.width(), CV_8UC4, (uchar*)qimg.constBits(), qimg.bytesPerLine());
        break;
    case QImage::Format_RGB888:
        mat = cv::Mat(qimg.height(), qimg.width(), CV_8UC3, (uchar*)qimg.constBits(), qimg.bytesPerLine());
        cv::cvtColor(mat, mat, cv::COLOR_BGR2RGB);
        break;
    case QImage::Format_Indexed8:
        mat = cv::Mat(qimg.height(), qimg.width(), CV_8UC1, (uchar*)qimg.constBits(), qimg.bytesPerLine());
        break;
    default:
        qDebug() << "Unrecognised format.";
        exit(1);
    }
    return mat;
}

void ImgWindow::set_img_brightness(){
    if (img.isNull()){
        qInfo() << "No image loaded yet";
        brightness_slider->setValue(0);
        return;
    }
    qInfo() << QString::fromStdString("size of stack:" + std::to_string(image_stack.size()));

    img_brightness = brightness_slider->value();
    qInfo() << img_brightness;
    cv::Mat cv_img = QImg_to_cvImg(img);
    cv_img.convertTo(cv_img, -1, 1, img_brightness);
    img = cvImg_to_QImg(cv_img);
    brightness_slider->setValue(0);
    qInfo() << QString::fromStdString("size of stack:" + std::to_string(image_stack.size()));

    set_image();
}

void ImgWindow::set_slider(){
    brightness_slider->setMinimum(-100);
    brightness_slider->setMaximum(100);
    brightness_slider->setValue(0);
}

void ImgWindow::set_image(){
    image_stack.push(img);
    qInfo() << QString::fromStdString("size of stack:" + std::to_string(image_stack.size()));
    QPixmap pm = QPixmap::fromImage(img);
    img_label->setPixmap(pm);
    img_label->setScaledContents(true);
}

void ImgWindow::set_image(bool exists){
    if (exists){
        QPixmap pm = QPixmap::fromImage(img);
        img_label->setPixmap(pm);
        img_label->setScaledContents(true);
    } else{
        img_label->setText("No previous edition.");
        }
}

void ImgWindow::set_previous_image(){
    if (! image_stack.empty()){
        image_stack.pop();
        img = image_stack.top();
        qInfo() << QString::fromStdString("size of stack:" + std::to_string(image_stack.size()));
        set_image(true);
    }
    else {
        set_image(false);
        qInfo() << "Empty stack. No items pushed to stack";
    }
    return;
}
